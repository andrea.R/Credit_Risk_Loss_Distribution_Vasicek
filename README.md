# Simulating the loss distribution of a loans portfolio under the Vasicek one-factor model
The single-factor Vasicek model provides a very basic description of the causes underpinning the "health status" of a loan.
In the banking context, the lending firm administers a pool of risky loans and should effectively estimate the degree of credit risk which it's exposed to. This is not only important *per se* for the profitability and survival of the institution but it's also mandatory in order to comply with the *Basel regulatory framework*.
Here, we exploit the Vasicek model to simulate the credit loss of a loans portfolio under different scenarios, and draw the resulting distribution. From the latter, we can extract all the risk metrics that we want, such as the VaR or the Expected Shortfall (ES); the so called Expected Loss (EL) and Unexpected Loss (UL) are particulary relevant for the *Basel requirements*.
We then fit the "empirical" distribution to some of the most popular statistical distribution used in the credit risk domain. 

## Future developments
 - Establish a comparison with real-world loans data.
 - Extend the analysis and data inspection to the insurance domain: the risky loans could be turned into the claims the insurance company must eventually pay to the insurers in exchange for the collected premiums. *Basel* --> *Solvency*. We can then play with actuarial models.

## Exploited libraries
 - Matplotlib
 - Numpy
 - Pandas
 - Scipy
